import Vue from 'vue'
import Router from 'vue-router'
import UserLogin from '@/components/UserLogin'
import StudentList from '@/components/StudentList'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'UserLogin',
      component: UserLogin
    },
    {
      path: '/StudentList',
      name: 'StudentList',
      component: StudentList
    }
  ]
})
